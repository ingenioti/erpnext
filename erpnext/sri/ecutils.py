from __future__ import unicode_literals
import xml.etree.cElementTree as ET
import frappe, os
import random

#def pin_dv_gen(digs):
#    l = len(digs)
#    d=0
#    for i in range(1,l+1):
#        d = d + int(digs[-i])*(i+1)
#        dv=d*10 % 11
#    if 10==dv: return '0'
#    return str( dv )

def pin_dv_gen(digs):
    baseMultiplicador = 7
    multiplicador = 2
    verificador = 0
    total = 0
    for i in  reversed(digs):
        aux = int(i) * multiplicador
        multiplicador = multiplicador + 1
        if multiplicador > baseMultiplicador:
            multiplicador = 2
        total+=aux

    if total < 0 :
        verificador = 0
    else:
        m = total % 11

        r = 11 - m
        if r == 11:
            verificador = 0
        else:
            verificador = r

    if verificador == 10 :
            verificador = 1

    return str(verificador)


def pin_dv_verify(digs):
    dig = pin_dv_gen(digs[0:-1])
    return dig == digs[-1]

def generate_pin_with_dv():
    pin = "".join(random.sample('0123456789', 8))
    return pin + pin_dv_gen(pin)

def generarClaveAcesso(fecha,tipoComprobante,ruc,ambiente,serie,secuencial):
    arrayFecha = fecha.split('-')
    dia = arrayFecha[2]
    mes = arrayFecha[1]
    anio = arrayFecha[0]
    claveAcceso = dia + mes + anio
    pin = "".join(random.sample('0123456789', 8))
    claveAcceso48 = (claveAcceso + tipoComprobante + ruc +
    ambiente + serie + secuencial + pin + '1')
    claveAcceso = claveAcceso48 + pin_dv_gen(claveAcceso48)
    return claveAcceso

def genenarXmlFacturas(clave_acceso):
    #ruta = os.path.dirname(__file__)
    #ruta = frappe.local.site
    ruta = os.path.abspath(frappe.get_site_path("public", "files"))
    directorio = ruta + "/xmls"
    rutafile = directorio + "/" + clave_acceso + ".xml"

    if not os.path.exists(directorio):
        os.makedirs(directorio)

    ruc = clave_acceso[10:-26]

    factura = ET.Element('factura')
    factura.set("id", "comprobante")
    factura.set("version", "1.1.0")
    infoTributaria = ET.SubElement(factura, "infoTributaria")
    ambiente = ET.SubElement(infoTributaria, "ambiente")
    ambiente.text = clave_acceso[23:-25]
    tipoEmision = ET.SubElement(infoTributaria, "tipoEmision")
    tipoEmision.text = clave_acceso[47:-1]
    razonSocial = ET.SubElement(infoTributaria, "razonSocial")
    razonSocial.text = getRazonSocial(ruc)
    nombreComercial = ET.SubElement(infoTributaria, "nombreComercial")
    nombreComercial.text = getNombreComercial(ruc)
    _ruc = ET.SubElement(infoTributaria, "ruc")
    _ruc.text = ruc
    claveAcceso = ET.SubElement(infoTributaria, "claveAcceso")
    claveAcceso.text = clave_acceso
    codDoc = ET.SubElement(infoTributaria, "codDoc")
    codDoc.text = clave_acceso[8:-39]
    estab = ET.SubElement(infoTributaria, "estab")
    estab.text = clave_acceso[24:-22]
    ptoEmi = ET.SubElement(infoTributaria, "ptoEmi")
    ptoEmi.text = clave_acceso[27:-19]
    secuencial = ET.SubElement(infoTributaria, "secuencial")
    secuencial.text = clave_acceso[30:-10]
    dirMatriz = ET.SubElement(infoTributaria, "dirMatriz")
    dirMatriz.text = getdirMatriz(ruc)

    infoFactura = ET.SubElement(factura, "infoFactura")
    fechaEmision = ET.SubElement(infoFactura, "fechaEmision")
    femi = (clave_acceso[0:-47] + '/' +
    clave_acceso[2:-45] + '/' + clave_acceso[4:-41])
    fechaEmision.text = femi

    dirEstablecimiento = ET.SubElement(infoFactura, "dirEstablecimiento")
    dirEstablecimiento.text = getdirEstablecimiento(clave_acceso[24:-22])

    objespecial = getEspecial(ruc)
    if not objespecial:
        contribuyenteEspecial = ET.SubElement(infoFactura,
             "contribuyenteEspecial")
        contribuyenteEspecial.text = objespecial[0][0]

    obligadoContabilidad = ET.SubElement(infoFactura, "obligadoContabilidad")
    obligadoContabilidad.text = getContabilidad(ruc)



    #guiaRemision = ET.SubElement(factura, "guiaRemision")
    #guiaRemision.text = ""
    datosComprador = getComprador(clave_acceso)
    tipoIdenComprador = datosComprador[0][0]
    rucComprador = datosComprador[0][1]
    nombreComprador = datosComprador[0][2]

    tipoI1,tipoI2= tipoIdenComprador.split('-')

    tipoIdentificacionComprador = ET.SubElement(infoFactura,"tipoIdentificacionComprador")
    tipoIdentificacionComprador.text = tipoI2

    razonSocialComprador = ET.SubElement(infoFactura, "razonSocialComprador")
    razonSocialComprador.text = nombreComprador

    identificacionComprador = ET.SubElement(infoFactura, "identificacionComprador")
    identificacionComprador.text = rucComprador


    #direccionComprador = ET.SubElement(infoFactura, "direccionComprador")
    #direccionComprador.text = rucComprador

    datosSubyDesc = getSubtotalyDesc(clave_acceso)

    totalSinImpuesto = ET.SubElement(infoFactura, "totalSinImpuestos")
    totalSinImpuesto.text = '{0:.2f}'.format(datosSubyDesc[0][1])

    totalDescuento = ET.SubElement(infoFactura, "totalDescuento")
    totalDescuento.text = '{0:.2f}'.format(datosSubyDesc[0][0])

    totalConImpuestos = ET.SubElement(infoFactura,"totalConImpuestos")
    totalImpuesto = ET.SubElement(totalConImpuestos,"totalImpuesto")

    codigo = ET.SubElement(totalImpuesto, "codigo")
    codigo.text = "2"

    codigoPorcentaje = ET.SubElement(totalImpuesto, "codigoPorcentaje")
    codigoPorcentaje.text = getCodigoImpuestoSri(clave_acceso)

    descuentoAdicional = ET.SubElement(totalImpuesto, "descuentoAdicional")
    descuentoAdicional.text = '{0:.2f}'.format(datosSubyDesc[0][0])

    baseImponible = ET.SubElement(totalImpuesto, "baseImponible")
    baseImponible.text = '{0:.2f}'.format(datosSubyDesc[0][1])

    valor = ET.SubElement(totalImpuesto, "valor")
    valor.text = '{0:.2f}'.format(datosSubyDesc[0][2])

    propina = ET.SubElement(infoFactura, "propina")
    propina.text = "0.00"

    importeTotal = ET.SubElement(infoFactura, "importeTotal")
    importeTotal.text = '{0:.2f}'.format(datosSubyDesc[0][3])

    moneda = ET.SubElement(infoFactura, "moneda")
    moneda.text = "DOLAR"

    formapago = str(datosSubyDesc[0][4])
    txt,codfp = formapago.split('-')
    pagos  = ET.SubElement(infoFactura, "pagos")
    pago = ET.SubElement(pagos, "pago")
    formaPago = ET.SubElement(pago, "formaPago")
    formaPago.text = codfp
    total = ET.SubElement(pago, "total")
    total.text =  '{0:.2f}'.format(datosSubyDesc[0][3])

    detalles = ET.SubElement(factura, "detalles")
    detalle = ET.SubElement(detalles, "detalle")
    productos = getProductos(clave_acceso)
    for p in productos:
        codigoPrincipal = ET.SubElement(detalle, "codigoPrincipal")
        codigoPrincipal.text = p[5]

        codigoAuxiliar = ET.SubElement(detalle, "codigoAuxiliar")
        codigoAuxiliar.text = p[5]

        descripcion = ET.SubElement(detalle, "descripcion")
        descripcion.text = p[1]

        cantidad = ET.SubElement(detalle, "cantidad")
        cantidad.text = '{0:.2f}'.format(p[3])

        precioUnitario = ET.SubElement(detalle, "precioUnitario")
        precioUnitario.text = '{0:.2f}'.format(p[2])

        descuento = ET.SubElement(detalle, "descuento")
        descuento.text = '{0:.2f}'.format(p[4])

        precioTotalSinImpuesto = ET.SubElement(detalle, "precioTotalSinImpuesto")
        precioTotalSinImpuesto.text = '{0:.2f}'.format(p[0])

        impuestos_prod =  ET.SubElement(detalle, "impuestos")
        impuesto_prod =  ET.SubElement(impuestos_prod, "impuesto")
        codigo_prod =  ET.SubElement(impuesto_prod, "codigo")
        codigo_prod.text = "2"

        codigoPorcentaje_prod =  ET.SubElement(impuesto_prod, "codigoPorcentaje")
        codigoPorcentaje_prod.text = "2"

        tarifa_prod =  ET.SubElement(impuesto_prod, "tarifa")
        tarifa_prod.text = "12"

        baseImponible_prod =  ET.SubElement(impuesto_prod, "baseImponible")
        baseImponible_prod.text =  '{0:.2f}'.format(p[6])

        valor_prod =  ET.SubElement(impuesto_prod, "valor")
        valor_prod.text =  '{0:.2f}'.format(p[7])


    infoAdicional =  ET.SubElement(factura, "infoAdicional")
    campoAdicional =  ET.SubElement(infoAdicional, "campoAdicional")
    campoAdicional.text = "correo"
    campoAdicional.set('nombre', 'mail')

    tree = ET.ElementTree(factura)
    tree.write(rutafile, xml_declaration=True, encoding='utf-8', method="xml")

    return rutafile

def getRazonSocial(ruc):
    obj_rz = frappe.db.sql("""select  razonsocial  from tabEmisor where ruc = %s""",ruc)
    if not obj_rz:
        frappe.throw("No se ha establecido la Razon Social")
    sri_rz = obj_rz[0][0]
    return sri_rz

def getNombreComercial(ruc):
    obj_nc = frappe.db.sql("""select  nombrecomercial  from tabEmisor where ruc = %s""",ruc)
    if not obj_nc:
        #frappe.throw("No se ha establecido la Razon Social")
        sri_nc=getRazonSocial(ruc)
    sri_nc = obj_nc[0][0]
    return sri_nc

def getdirMatriz(ruc):
    obj_rm = frappe.db.sql("""select  dir_est_mat  from tabEmisor where ruc = %s""",ruc)
    if not obj_rm:
        frappe.throw("No se ha establecido la dirMatriz")
    sri_rm = obj_rm[0][0]
    return sri_rm

def getdirEstablecimiento(estab):
    sqldirest = "select address_line1  from tabAddress where name in(select parent  from `tabDynamic Link`    where link_doctype = 'Warehouse' and link_name in (select name  from tabWarehouse where establecimiento='{0}'))".format(estab)
    obj_direst = frappe.db.sql(sqldirest )
    if not obj_direst:
        frappe.throw("No se ha la direccion del establecimiento"+estab )
    sri_direst = obj_direst[0][0]
    return sri_direst

def getContabilidad(ruc):
    obj_conta = frappe.db.sql("""SELECT contabilidad   from tabEmisor where ruc =%s""",ruc)
    sri_conta = obj_conta[0][0]
    return sri_conta

def getEspecial(ruc):
    obj_especial = frappe.db.sql("""SELECT  especial  from tabEmisor where ruc =%s""",ruc)
    return obj_especial

def getComprador(clave_acceso):
    obj_comprador = frappe.db.sql("""select  tipoidentificacion,dni,razonsocial
    from tabCustomer where name in (select customer from `tabSales Invoice`
    WHERE sri_claveacceso =%s)""",clave_acceso)
    return obj_comprador

def getSubtotalyDesc(clave_acceso):
    obj_select = frappe.db.sql("""select discount_amount,net_total,base_total_taxes_and_charges,grand_total , formapago from `tabSales Invoice` WHERE sri_claveacceso =%s""",clave_acceso)
    return obj_select

def getCodigoImpuestoSri(clave_acceso):
    obj_select = frappe.db.sql("""select codigo_sri from `tabSales Taxes and Charges Template` where name in ( select taxes_and_charges from `tabSales Invoice` WHERE sri_claveacceso=%s )""",clave_acceso)
    resultado= obj_select[0][0]
    return resultado

def getPorcentajeImpuestoSri(clave_acceso):
    obj_select = frappe.db.sql("""select  rate  from  `tabSales Taxes and Charges` WHERE parent   in ( select taxes_and_charges from `tabSales Invoice` WHERE sri_claveacceso='%s' )""",clave_acceso)
    resultado= obj_select[0][0]
    return resultado

def getProductos(clave_acceso):
    productos = frappe.db.sql(""" select  (qty * (base_rate -discount_amount)) as total,
         item_name,
         base_rate ,
         qty,
         discount_amount,
         item_code ,
          (base_rate -discount_amount) as subtotal,
           (base_rate -discount_amount) *0.12 as iva  from `tabSales Invoice Item` where parent in ( select name from `tabSales Invoice`  where sri_claveacceso=%s  ) """,(clave_acceso))
    return productos



